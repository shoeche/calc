#include "Algebra_Interpreter.H"
#include "Tools.H"

using namespace ATOOLS;

int ATOOLS::s_olevel(0);
int ATOOLS::s_indent(0);

void PrintHelp()
{
  std::cout<<"\n Calc Version 1.0\n\n"
	   <<" options:\t-v           verbose. print input and expression\n"
	   <<"\t\t-h           display this help and exit\n\n";
}

int main(int argc,char **argv)
{
  Algebra_Interpreter interpreter;
  std::string expr;
  for (int i=1;i<argc;++i) {
    std::string argvs=argv[i];
    if (argvs[0]=='-' && argvs.length()==2) {
      if (argvs[1]=='v') { s_olevel=47; continue; }
      if (argvs[1]=='h') { PrintHelp(); exit(0); }
    }
    size_t pos=argvs.find("=");
    if (pos!=std::string::npos && argvs.length()>pos && 
	pos>0 && argvs[pos+1]!='=' && argvs[pos-1]!='!' 
	&& argvs[pos-1]!='<' && argvs[pos-1]!='>')
      interpreter.AddTag(argvs.substr(0,pos),argvs.substr(pos+1));
    else expr+=argv[i];
  }
  std::string result(interpreter.Interprete(expr));
  if (s_olevel==0) std::cout<<result<<std::endl;
  return 0;
}
