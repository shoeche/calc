#!/usr/bin/python

import os;
import sys;

es = {'1':'1'};

# tag replacement
es['a+b \'a=(c,2)\' \'b=(3,d)\' c=1 d=4'] = '(4,6)';

# binary operators
es['\'(2+2*2)-(2-4/2)\''] = '6';
es['\'(2<<1)+(4>>1)+3%2\''] = '7';
es['\'(2>1)+2*(2<2)+4*(2<2)+8*(2<3)\''] = '9';
es['\'(2>=3)+2*(2<=2)+4*(2<=2)+8*(2<=1)\''] = '6';
es['\'(2==1+1)+2*(2!=1)*(6&10)|2\''] = '7';
es['\'(2>1&&2<2)+2*(2<=2||2>=1)\''] = '2';
# unary minus
es['\'exp(-1)*exp(1)\''] = '1';
es['\'-(-(-2e-1)-(1|2)*-1e-1)+0.5\''] = '0';
es['\'-(1,-1)-(-1,1)*exp(-1+(3&5))\''] = '(0,0)';
# real functions
es['\'min(1,2)+max(3,4,5)\''] = '6';
es['\'exp(1)+log(2.71828182846)\''] = '3.71828182846';
es['\'log10(10)+pow(10,1)\''] = '11';
es['\'sqr(2)+sqrt(4)\''] = '6';
es['\'sgn(-2)+abs(-2)\''] = '1';
es['\'sin(M_PI/2)-cos(M_PI)+tan(M_PI/4)\''] = '3';
es['\'(asin(1)-acos(-1))/2+atan(1)\''] = '0';

print 'perform tests ',;
res=1; n=0;
for e in es:
   pf=os.popen('./Calc '+e+' 2>/dev/null','r');
   cr=pf.readline().rstrip('\n');
   cres=cr==es[e];
   if cres: 
       sys.stdout.write('.');
       sys.stdout.flush();
   else:
       print '\n\033[31mfailed\033[0m ',e,' ---> ',es[e],', got ',cr;
       res=0;
       break;
   n=n+1;

if res!=0: 
    print ' \033[32mok\033[0m';
