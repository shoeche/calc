CC = g++
INSTALL_PROGRAM = /usr/bin/install -c
exec_prefix = $(prefix)

Calc_SOURCES = Vector.C Term.C Algebra_Interpreter.C Calc.C
Calc_HEADERS = Tools.H Node.H Node.C Vector.H Term.H Algebra_Interpreter.H

EXTRA_DIST = COPYING calc_test.sh

CXXFLAGS = -O2 -DUSING__Calc_only -DDEBUG__Interpreter

Calc_DISTDIR = Calc-1.0

all: Calc

Calc: $(addsuffix .o,$(basename $(Calc_SOURCES)))

clean: 
	rm -f *.o Calc

dist:
	mkdir $(Calc_DISTDIR) && \
	cp $(Calc_SOURCES) $(Calc_HEADERS) \
	   $(EXTRA_DIST) Makefile $(Calc_DISTDIR) && \
	tar -czf $(Calc_DISTDIR).tar.gz $(Calc_DISTDIR) && \
	rm -r $(Calc_DISTDIR)

install:
	$(INSTALL_PROGRAM) -D Calc $(exec_prefix)/bin/Calc

